'use strict'
var massive = [];
function addGoods(a,b,c) {
    var object = {}; //создаем объект
    object.name = a; // приравниваем к name аргументы функции
    object.price = b; // приравниваем к price аргументы функции
    object.category = c; // приравниваем к category аргументы функции
    massive.push(object); // добавляем объект в конец массива
    return massive; // возвращает полученный массив
}
function filterItemsbyPrice(min, max) {
        min = prompt('min');
        max = prompt('max');
    var filterGoodsbyPrice =  massive.filter(function(item) { // возвращает товары заданного ценового диапазона
        return (item.price >= min && item.price <= max);
    });  
    return filterGoodsbyPrice;  
}

function filterItemsbyCat(categ) {
    categ = prompt('Enter categoty');
    var filterGoodsinCat =  massive.filter(function(item) { // выводит количество товаров заданной категории
        return item.category == categ;
    });
    var sum = filterGoodsinCat.length;
    return sum;
}
function filterByCat(cat) {
    cat = prompt('Enter category'); // возвращает массив с товарами по выбранной категории (в задании не было такой функции) 
    var filterGoodsinCat = massive.filter(function(item) {
        return item.category == cat;
    });
    return  filterGoodsinCat;
}


function deleteByName(itemName) {
    var itemName = prompt('Enter what you want to delete')
    var newArr = massive.filter(function(item) {//удаление элемента по имени
        if(item.name != itemName) {
            return true;
        }
    });
    return newArr;
}
function compareMinMax(a, b) {
    if (a.price > b.price) return 1;// вспомогательная функция настройки алгоритма сортировки
    if (a.price < b.price) return -1;
}
function compareMaxMin(a, b) {
    if (a.price < b.price) return 1;// вспомогательная функция настройки алгоритма сортировки
    if (a.price > b.price) return -1;
}
function sortMinMax(arg) { 
    var sortArr = arg.sort(compareMinMax)//cортирует от меньшего к большему
    return (sortArr);       
}
function sortMaxMin(arg) { 
    var sortArr = arg.sort(compareMaxMin)//cортирует от большему к меньшего 
    return (sortArr);       
}
function sortAndrange(sortOption,typesort) {  // функция, которая спрашивает
    //  у пользователя какой вид сортировки он хочет и по какому параметру: "ценовому диапазону" или "категории" и выводит
    var sortOption = prompt('Enter type of sort: MintoMax or MaxtoMin');
    var typesort = prompt('If you want sort by price, enter "price", if by category, enter "category" ');
    if (sortOption == 'MintoMax') {
        if (typesort == 'category') {
            return sortMinMax(filterByCat());
        }else if(typesort == 'price') {
            return sortMinMax(filterItemsbyPrice());
        }else alert('You write smth wrong');                 
        
    }else if (sortOption == 'MaxtoMin') {
        if (typesort == 'category') {
            return sortMaxMin(filterByCat());
        }else if(typesort == 'price') {
            return sortMaxMin(filterItemsbyPrice());
        }else alert('You write smth wrong');        
    }else alert ('You entered smth wrong');     
}
function sumOfPrices() {// функция, которая спрашивает какую выборку делать: по ценовому диапазону
    //  или категории и считает сумму цен товаров из данной выборки
    var typesort = prompt('If you want sort by price, enter "price", if by category, enter "category" ');
    if (typesort == 'category') {
        var sum = 0;
        var a;
        filterByCat().map(function(item) {
            a = item.price;
            sum += a;
          });
          return sum;
    }else if (typesort == 'price') {
        var sum = 0;
        var a;
        filterItemsbyPrice().forEach(function(item) {
            a = item.price;
            sum += a;
          });
          return sum;
          
    }else alert ('You entered smth wrong');
}
